﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema2Problema3
{
    enum GradProfesor
    {
        definitivat,
        grad_1,
        grad_2,
        doctorat
    }
    class Profesor : Persoana
    {
        public Profesor(String num,String prenum,String adr,GradProfesor gradProfesor):base(num,prenum,adr)
        {
            this.gradProfesor = gradProfesor;
        }

        public GradProfesor gradProfesor { get; set; }
    }
}
