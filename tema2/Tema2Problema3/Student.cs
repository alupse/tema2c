﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema2Problema3
{
    class Student : Persoana
    {
        public double medie { get; set; }
        public String nrMatricol { get; set; }
        public Student(string num, string prenum, string adr, double med, String nrMatr) : base(num, prenum ,adr)
        {
            medie = med;
            nrMatricol = nrMatr;
        }
    }
}
