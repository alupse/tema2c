﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Tema2Problema3
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Persoana> allPersons = new List<Persoana>();
            Persoana p1 = new Student("Nale","Clarisa","803 North Avenue Chicago Illinois",9.45,"NC12");
            Persoana p2 = new Student("Hill", "Jaclin", "8424 Sorento Street Chicago Illinois", 8.30, "HC34");
            Persoana p3 = new Student("Charles", "Amanda", "184 Kedzie Street Chicago Illinois", 7.45, "CA92");
            Persoana p4 = new Student("Grant", "Elizabeth", "788 South Avenue Chicago Illinois", 10, "GE99");
            Persoana p5 = new Profesor("Haslup", "Steve", "457  Dempster Street Chicago Illinois", GradProfesor.definitivat);
            Persoana p6 = new Profesor("Harrison", "John", "5454 Dempster Street Chicago Illinois", GradProfesor.grad_1);
            Persoana p7 = new Profesor("Fraser", "Joshua", "223  Acapulco Street Chicago Illinois", GradProfesor.doctorat);
            Persoana p8 = new Profesor("Brown", "Sarah", "6543  Alaskan Street Chicago Illinois", GradProfesor.definitivat);

            allPersons.Add(p1);
            allPersons.Add(p2);
            allPersons.Add(p3);
            allPersons.Add(p4);
            allPersons.Add(p5);
            allPersons.Add(p6);
            allPersons.Add(p7);
            allPersons.Add(p8);
           

            List<Persoana> allPersonsSorted =allPersons.OrderBy(p => p.prenume ).ToList();

            foreach (Persoana p in allPersonsSorted)
            {
               Console.WriteLine( p.ToString());
            }

            Console.WriteLine("=========================");
            allPersonsSorted = allPersons.OrderBy(p => p.nume).ToList();

            foreach (Persoana p in allPersonsSorted)
            {
                Console.WriteLine(p.ToString());
            }





        }
    }
}
