﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tema2Problema3
{
    public class Persoana
    {
        public String nume { get; set; }
        public String prenume { get; set; }
        public String adresa { get; set; }

        public Persoana(String num,String prenum,String adr)
        {
            nume = num;
            prenume = prenum;
            adresa = adr;
        }

        public override string ToString()
        {
            return nume + "  " +prenume + "   " + adresa;
            
            
        }
    }
}
